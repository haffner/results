2022-04-04
Analysis of Coll. 3 and Coll. 4 solar irradiance performed by Sergey Marchenko
Plots from TLCF @ /sctratchdev/smartche/download/coll3_4 

Coll. 4 v2077 and v2081 solar data (extracted from CAL files) compared to Coll. 3 irradiances (from *IR2 product) used to produce OMI irradiances delivered to solar community. Results show two consecutive years (2011,2012; 2019,2020) to assess repeatability of goniometric changes from year to year, which is an important factor in application of the OMI solar algorithm.